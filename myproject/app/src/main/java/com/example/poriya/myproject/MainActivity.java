package com.example.poriya.myproject;

import android.content.Intent;
import android.graphics.Typeface;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
//golobal
    private TextView txt_header;
    private Button btn_create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt_header=(TextView) findViewById(R.id.txt_header);
        Typeface typeface=Typeface.createFromAsset(getAssets(),"fonts/3ds Bold Italic.ttf");
        txt_header.setTypeface(typeface);
        ///-----------------
        btn_create=(Button) findViewById(R.id.btn_createaccount);
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ints=new Intent(MainActivity.this,RegActivity.class);
                startActivity(ints);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

